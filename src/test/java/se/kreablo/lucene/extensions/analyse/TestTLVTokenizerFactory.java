package se.kreablo.lucene.extensions.analyse;

/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.Reader;
import java.io.StringReader;

import org.apache.lucene.analysis.MockTokenizer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.util.BaseTokenStreamFactoryTestCase;

/**
 * Simple tests to ensure the TLV tokenizer factory is working.
 */
public class TestTLVTokenizerFactory extends BaseTokenStreamFactoryTestCase {
  /**
   * Test StandardTokenizerFactory
   */
  public void testTLVTokenizer() throws Exception {
    Reader reader = new StringReader("Wha\u0301t's this thing do?");
    TokenStream stream = tokenizerFactory("TLV").create(reader);
    assertTokenStreamContents(stream,
      new String[] { "Wha\u0301t's", "this",       "thing",      "do" },
      new String[] { "<ALPHANUM>",   "<ALPHANUM>", "<ALPHANUM>", "<ALPHANUM>" }
     );
  }

  public void testCodes() throws Exception {
    Reader reader = new StringReader("A.0 B.1 C.2 D.33 E.12414 F.1 108 G.2 108 5036");
    TokenStream stream = tokenizerFactory("TLV").create(reader);
    assertTokenStreamContents(stream,
                              new String[] { "A.0",          "B.1",          "C.2",          "D.33",         "E.12414",       "F.1",           "108",               "G",          "2 108",  "5036"},
                              new String[] { "<TERMS_CODE>", "<TERMS_CODE>", "<TERMS_CODE>", "<TERMS_CODE>", "<TERMS_CODE>",  "<TERMS_CODE>",  "<MEASURE_CODE>",    "<ALPHANUM>", "<NUM>",  "<CONDITION_CODE>" }
                              );
  }

  public void testNoCodes() throws Exception {
    Reader reader = new StringReader("A.0a aB.1 C3.2 3D.33 -E.12414 10811 50361 6001");
    TokenStream stream = tokenizerFactory("TLV").create(reader);
    assertTokenStreamContents(stream,
                              new String[] { "A.0",          "a",          "aB",         "1",     "C3.2",       "3D",         "33",    "E.12414",      "10811",      "50361",      "6001" },
                              new String[] { "<TERMS_CODE>", "<ALPHANUM>", "<ALPHANUM>", "<NUM>", "<ALPHANUM>", "<ALPHANUM>", "<NUM>", "<TERMS_CODE>", "<NUM>",     "<NUM>",      "<NUM>" }
                              );
  }

  public void testXWikiItems() throws Exception {
    Reader reader = new StringReader("image:foobar.png [[foobar]] [[foobar>>foobar]]  [[image:foobar>>foobar]] [[image:foobar]] imagefoobar [[label>>link]] ~[[label~>>link]]");
    TokenStream stream = tokenizerFactory("TLV").create(reader);
    assertTokenStreamContents(stream,
                              new String[] { "image:foobar.png", "foobar",     "foobar",     ">>foobar]]",   "[[image:foobar>>foobar]]",  "[[image:foobar]]", "imagefoobar", "label",      ">>link]]",   "label",      "link" },
                              new String[] { "<XWIKI_ITEM>",     "<ALPHANUM>", "<ALPHANUM>", "<XWIKI_ITEM>", "<XWIKI_ITEM>",            "<XWIKI_ITEM>",     "<ALPHANUM>",  "<ALPHANUM>", "<XWIKI_ITEM>", "<ALPHANUM>", "<ALPHANUM>"   });

  }

}
