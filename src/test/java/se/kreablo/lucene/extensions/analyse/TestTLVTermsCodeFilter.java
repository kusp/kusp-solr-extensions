package se.kreablo.lucene.extensions.analyse;

import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.BaseTokenStreamTestCase;
import org.apache.lucene.analysis.MockTokenizer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.core.KeywordTokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.util.Version;

public class TestTLVTermsCodeFilter extends BaseTokenStreamTestCase {

    /*
  public void testCode() throws Exception {
    String input = "C.34 foo";
    // first test
    //TokenStream ts = new MockTokenizer(new StringReader(input), MockTokenizer.WHITESPACE, false);

    TokenStream ts = new StandardTokenizer(Version.LUCENE_48, new StringReader(input));
    ts = new TLVTermsCodeFilter(ts);
    assertTokenStreamContents(ts,
      new String[] { "C", ".", "34", "foo" });
  }

  public void testCodeAtEnd() throws Exception {
      String input = "foo A.12 b.3 c.0 d.-1 e.100 ee.10 FF.12";
      // first test
      TokenStream ts = new MockTokenizer(new StringReader(input), MockTokenizer.WHITESPACE, false);
      ts = new TLVTermsCodeFilter(ts);
      assertTokenStreamContents(ts,
          new String[] { "foo", "A.12", "b.3", "c.0", "d", "1", "e100", "ee", "10", "FF", "12" });
  }

  public void testOffsets() throws Exception {
    String input = "foo A.02 B.3 c.0 bar";
    TokenStream ts = new MockTokenizer(new StringReader(input), MockTokenizer.WHITESPACE, false);
    ts = new TLVTermsCodeFilter(ts);
    assertTokenStreamContents(ts, 
        new String[] { "foo", "A.02", "B.3", "c.0", "bar" },
        new int[] { 0, 4, 9, 13, 18 },
        new int[] { 3, 8, 12, 17, 20 });
  }
*/
  /** blast some random strings through the analyzer */
  public void testRandomString() throws Exception {
    Analyzer a = new Analyzer() {

      @Override
      protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
        Tokenizer tokenizer = new MockTokenizer(reader, MockTokenizer.WHITESPACE, false);
        return new TokenStreamComponents(tokenizer, new TLVTermsCodeFilter(tokenizer));
      }
    };
    
    checkRandomData(random(), a, 1000*RANDOM_MULTIPLIER);
  }
  
  public void testEmptyTerm() throws IOException {
    Analyzer a = new Analyzer() {
      @Override
      protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
        Tokenizer tokenizer = new KeywordTokenizer(reader);
        return new TokenStreamComponents(tokenizer, new TLVTermsCodeFilter(tokenizer));
      }
    };
    checkOneTerm(a, "", "");
  }
}
