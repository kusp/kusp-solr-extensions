package se.kreablo.lucene.extensions.analyse;


/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import org.apache.lucene.analysis.tokenattributes.CharTermAttribute;
import org.apache.lucene.analysis.standard.StandardTokenizer;
import org.apache.lucene.analysis.standard.StandardTokenizerInterface;

/**
 * This class implements Word Break rules from the Unicode Text Segmentation
 * algorithm, as specified in
 * <a href="http://unicode.org/reports/tr29/">Unicode Standard Annex #29</a>.
 * <p/>
 * Tokens produced are of the following types:
 * <ul>
 *   <li>&lt;ALPHANUM&gt;: A sequence of alphabetic and numeric characters</li>
 *   <li>&lt;NUM&gt;: A number</li>
 *   <li>&lt;SOUTHEAST_ASIAN&gt;: A sequence of characters from South and Southeast
 *       Asian languages, including Thai, Lao, Myanmar, and Khmer</li>
 *   <li>&lt;IDEOGRAPHIC&gt;: A single CJKV ideographic character</li>
 *   <li>&lt;HIRAGANA&gt;: A single hiragana character</li>
 *   <li>&lt;KATAKANA&gt;: A sequence of katakana characters</li>
 *   <li>&lt;HANGUL&gt;: A sequence of Hangul characters</li>
 * </ul>
 */
%%

%unicode 6.3
%integer
%final
%public
%class TLVTokenizerImpl
%implements StandardTokenizerInterface
%function getNextToken
%char
%buffer 255
%state NON_XWIKI

TLVTermsCode = ([abcdefABCDEF]\.[:digit:]+)
TLVQuestionCode = (F[:digit:]+)
TLVAdviceCode = (AR[:digit:]+)
TLVConditionCode = ((1301)|(3051)|(1001)|(1302)|(2061)|(3021)|(2021)|(2041)|(2051)|(2071)|(3043)|(3045)|(3022)|(3041)|(3042)|(3044)|(3046)|(3063)|(3064)|(3065)|(3061)|(3062)|(3071)|(3072)|(3073)|(3111)|(3121)|(3122)|(3151)|(3161)|(3162)|(4001)|(4071)|(4079)|(4002)|(4011)|(4012)|(4072)|(4073)|(4074)|(4075)|(4076)|(4077)|(4078)|(4080)|(4081)|(4471)|(4882)|(4771)|(4772)|(5001)|(5010)|(4883)|(4884)|(5002)|(5005)|(5011)|(5015)|(5016)|(5012)|(5013)|(5014)|(5020)|(5031)|(5034)|(5033)|(5037)|(5035)|(5036)|(5045)|(5046)|(5051)|(5052)|(5041)|(5042)|(5061)|(5071)|(5062)|(5445)|(5447)|(5072)|(5443)|(5444)|(5448)|(5450)|(5451)|(5903)|(5905)|(5449)|(5906)|(5907)|(5909)|(5908)|(5910)|(5912)|(5911)|(5913)|(5914))
TLVMeasureCode = ((108)|(101)|(103)|(107)|(112)|(125)|(126)|(131)|(132)|(133)|(141)|(161)|(162)|(163)|(164)|(201)|(205)|(206)|(301)|(313)|(314)|(321)|(322)|(341)|(111)|(113)|(114)|(115)|(116)|(121)|(122)|(123)|(124)|(134)|(204)|(302)|(303)|(311)|(312)|(342)|(404)|(405)|(406)|(407)|(424)|(427)|(428)|(429)|(431)|(432)|(435)|(436)|(442)|(443)|(445)|(446)|(447)|(448)|(480)|(343)|(362)|(401)|(402)|(403)|(420)|(421)|(422)|(423)|(425)|(426)|(430)|(441)|(444)|(502)|(503)|(504)|(542)|(601)|(602)|(604)|(607)|(701)|(706)|(707)|(806)|(807)|(808)|(822)|(823)|(824)|(825)|(827)|(501)|(521)|(522)|(523)|(541)|(603)|(606)|(702)|(703)|(704)|(705)|(708)|(800)|(801)|(802)|(803)|(804)|(805)|(809)|(811)|(812)|(813)|(814)|(815)|(826)|(828)|(829)|(831)|(832)|(835)|(836)|(837)|(845)|(856)|(857)|(858)|(861)|(862)|(863)|(882)|(889)|(901)|(902)|(903)|(921)|(926)|(928)|(929)|(941)|(833)|(834)|(839)|(846)|(847)|(848)|(850)|(852)|(853)|(854)|(855)|(865)|(871)|(872)|(873)|(874)|(875)|(876)|(877)|(878)|(880)|(881)|(883)|(884)|(888)|(904)|(905)|(906)|(907)|(908)|(922)|(925)|(940))
XWikiLinkTarget = >>[^\R\]]+\]\]
XWikiImage = ((\[\[)image:[^\s\]]+((\]\])))|(image:[^\s]+)
XWikiAttrs = (%[^\R]*%)
XWikiMacro = \{\{[^\}\R]+\}\}
XWikiEscape = \~

// UAX#29 WB4. X (Extend | Format)* --> X
//
HangulEx            = [\p{Script:Hangul}&&[\p{WB:ALetter}\p{WB:Hebrew_Letter}]] [\p{WB:Format}\p{WB:Extend}]*
HebrewOrALetterEx   = [\p{WB:HebrewLetter}\p{WB:ALetter}]                       [\p{WB:Format}\p{WB:Extend}]*
NumericEx           = [\p{WB:Numeric}[\p{Blk:HalfAndFullForms}&&\p{Nd}]]        [\p{WB:Format}\p{WB:Extend}]*
KatakanaEx          = \p{WB:Katakana}                                           [\p{WB:Format}\p{WB:Extend}]*
MidLetterEx         = [\p{WB:MidLetter}\p{WB:MidNumLet}\p{WB:SingleQuote}]      [\p{WB:Format}\p{WB:Extend}]*
MidNumericEx        = [\p{WB:MidNum}\p{WB:MidNumLet}\p{WB:SingleQuote}]         [\p{WB:Format}\p{WB:Extend}]*
ExtendNumLetEx      = \p{WB:ExtendNumLet}                                       [\p{WB:Format}\p{WB:Extend}]*
HanEx               = \p{Script:Han}                                            [\p{WB:Format}\p{WB:Extend}]*
HiraganaEx          = \p{Script:Hiragana}                                       [\p{WB:Format}\p{WB:Extend}]*
SingleQuoteEx       = \p{WB:Single_Quote}                                       [\p{WB:Format}\p{WB:Extend}]*
DoubleQuoteEx       = \p{WB:Double_Quote}                                       [\p{WB:Format}\p{WB:Extend}]*
HebrewLetterEx      = \p{WB:Hebrew_Letter}                                      [\p{WB:Format}\p{WB:Extend}]*
RegionalIndicatorEx = \p{WB:RegionalIndicator}                                  [\p{WB:Format}\p{WB:Extend}]*
ComplexContextEx    = \p{LB:Complex_Context}                                    [\p{WB:Format}\p{WB:Extend}]*

%{
  /** Alphanumeric sequences */
  public static final int WORD_TYPE = StandardTokenizer.ALPHANUM;

  /** Numbers */
  public static final int NUMERIC_TYPE = StandardTokenizer.NUM;

  /**
   * Chars in class \p{Line_Break = Complex_Context} are from South East Asian
   * scripts (Thai, Lao, Myanmar, Khmer, etc.).  Sequences of these are kept
   * together as as a single token rather than broken up, because the logic
   * required to break them at word boundaries is too complex for UAX#29.
   * <p>
   * See Unicode Line Breaking Algorithm: http://www.unicode.org/reports/tr14/#SA
   */
  public static final int SOUTH_EAST_ASIAN_TYPE = StandardTokenizer.SOUTHEAST_ASIAN;

  public static final int IDEOGRAPHIC_TYPE = StandardTokenizer.IDEOGRAPHIC;

  public static final int HIRAGANA_TYPE = StandardTokenizer.HIRAGANA;

  public static final int KATAKANA_TYPE = StandardTokenizer.KATAKANA;

  public static final int HANGUL_TYPE = StandardTokenizer.HANGUL;

  public static final int TLV_TERMS_CODE_TYPE = TLVTokenizer.TERMS_CODE;

  public static final int TLV_QUESTION_CODE_TYPE = TLVTokenizer.QUESTION_CODE;

  public static final int TLV_ADVICE_CODE_TYPE = TLVTokenizer.ADVICE_CODE;

  public static final int TLV_MEASURE_CODE_TYPE = TLVTokenizer.MEASURE_CODE;

  public static final int TLV_CONDITION_CODE_TYPE = TLVTokenizer.CONDITION_CODE;

  public static final int XWIKI_ITEM_TYPE = TLVTokenizer.XWIKI_ITEM;

  public final int yychar()
  {
    return yychar;
  }

  /**
   * Fills CharTermAttribute with the current token text.
   */
  public final void getText(CharTermAttribute t) {
    t.copyBuffer(zzBuffer, zzStartRead, zzMarkedPos-zzStartRead);
  }

  /**
   * Sets the scanner buffer size in chars
   */
   @Override
   public void setBufferSize(int numChars) {
      throw new UnsupportedOperationException();
   }

   private void h() {
       if (yystate() == NON_XWIKI) {
           yybegin(YYINITIAL);
       }
   }
%}

%%

// UAX#29 WB1.   sot   ÷
//        WB2.     ÷   eot
//
<<EOF>> { return StandardTokenizerInterface.YYEOF; }

{TLVTermsCode}               { h(); return TLV_TERMS_CODE_TYPE; }
{TLVQuestionCode}            { h(); return TLV_QUESTION_CODE_TYPE; }
{TLVAdviceCode}              { h(); return TLV_ADVICE_CODE_TYPE; }
{TLVMeasureCode}             { h(); return TLV_MEASURE_CODE_TYPE; }
{TLVConditionCode}           { h(); return TLV_CONDITION_CODE_TYPE; }
// Allow large numerics on the form 12 108 234
{NumericEx}+(\W{NumericEx}{3})+/\W { h(); return NUMERIC_TYPE; }
{XWikiEscape} { yybegin(NON_XWIKI); }
<YYINITIAL> {XWikiLinkTarget}|{XWikiImage}|{XWikiAttrs}|{XWikiMacro}     { return XWIKI_ITEM_TYPE; }

// UAX#29 WB8.   Numeric × Numeric
//        WB11.  Numeric (MidNum | MidNumLet | Single_Quote) × Numeric
//        WB12.  Numeric × (MidNum | MidNumLet | Single_Quote) Numeric
//        WB13a. (ALetter | Hebrew_Letter | Numeric | Katakana | ExtendNumLet) × ExtendNumLet
//        WB13b. ExtendNumLet × (ALetter | Hebrew_Letter | Numeric | Katakana)
//
{ExtendNumLetEx}* {NumericEx} ( ( {ExtendNumLetEx}* | {MidNumericEx} ) {NumericEx} )* {ExtendNumLetEx}*
  { h(); return NUMERIC_TYPE; }

// subset of the below for typing purposes only!
{HangulEx}+
  { h(); return HANGUL_TYPE; }

{KatakanaEx}+
  { h(); return KATAKANA_TYPE; }

// UAX#29 WB5.   (ALetter | Hebrew_Letter) × (ALetter | Hebrew_Letter)
//        WB6.   (ALetter | Hebrew_Letter) × (MidLetter | MidNumLet | Single_Quote) (ALetter | Hebrew_Letter)
//        WB7.   (ALetter | Hebrew_Letter) (MidLetter | MidNumLet | Single_Quote) × (ALetter | Hebrew_Letter)
//        WB7a.  Hebrew_Letter × Single_Quote
//        WB7b.  Hebrew_Letter × Double_Quote Hebrew_Letter
//        WB7c.  Hebrew_Letter Double_Quote × Hebrew_Letter
//        WB9.   (ALetter | Hebrew_Letter) × Numeric
//        WB10.  Numeric × (ALetter | Hebrew_Letter)
//        WB13.  Katakana × Katakana
//        WB13a. (ALetter | Hebrew_Letter | Numeric | Katakana | ExtendNumLet) × ExtendNumLet
//        WB13b. ExtendNumLet × (ALetter | Hebrew_Letter | Numeric | Katakana)
//
{ExtendNumLetEx}*  ( {KatakanaEx}          ( {ExtendNumLetEx}*   {KatakanaEx}                           )*
                   | ( {HebrewLetterEx}    ( {SingleQuoteEx}     | {DoubleQuoteEx}  {HebrewLetterEx}    )
                     | {NumericEx}         ( ( {ExtendNumLetEx}* | {MidNumericEx} ) {NumericEx}         )*
                     | {HebrewOrALetterEx} ( ( {ExtendNumLetEx}* | {MidLetterEx}  ) {HebrewOrALetterEx} )*
                     )+
                   )
({ExtendNumLetEx}+ ( {KatakanaEx}          ( {ExtendNumLetEx}*   {KatakanaEx}                           )*
                   | ( {HebrewLetterEx}    ( {SingleQuoteEx}     | {DoubleQuoteEx}  {HebrewLetterEx}    )
                     | {NumericEx}         ( ( {ExtendNumLetEx}* | {MidNumericEx} ) {NumericEx}         )*
                     | {HebrewOrALetterEx} ( ( {ExtendNumLetEx}* | {MidLetterEx}  ) {HebrewOrALetterEx} )*
                     )+
                   )
)*
{ExtendNumLetEx}*
  { h(); return WORD_TYPE; }


// From UAX #29:
//
//    [C]haracters with the Line_Break property values of Contingent_Break (CB),
//    Complex_Context (SA/South East Asian), and XX (Unknown) are assigned word
//    boundary property values based on criteria outside of the scope of this
//    annex.  That means that satisfactory treatment of languages like Chinese
//    or Thai requires special handling.
//
// In Unicode 6.3, only one character has the \p{Line_Break = Contingent_Break}
// property: U+FFFC ( ￼ ) OBJECT REPLACEMENT CHARACTER.
//
// In the ICU implementation of UAX#29, \p{Line_Break = Complex_Context}
// character sequences (from South East Asian scripts like Thai, Myanmar, Khmer,
// Lao, etc.) are kept together.  This grammar does the same below.
//
// See also the Unicode Line Breaking Algorithm:
//
//    http://www.unicode.org/reports/tr14/#SA
//
{ComplexContextEx}+ { h(); return SOUTH_EAST_ASIAN_TYPE; }

// UAX#29 WB14.  Any ÷ Any
//
{HanEx} { h(); return IDEOGRAPHIC_TYPE; }
{HiraganaEx} { h(); return HIRAGANA_TYPE; }


// UAX#29 WB3.   CR × LF
//        WB3a.  (Newline | CR | LF) ÷
//        WB3b.  ÷ (Newline | CR | LF)
//        WB13c. Regional_Indicator × Regional_Indicator
//        WB14.  Any ÷ Any
//
{RegionalIndicatorEx} {RegionalIndicatorEx}+ | [^]
  { h(); /* Break so we don't hit fall-through warning: */ break; /* Not numeric, word, ideographic, hiragana, or SE Asian -- ignore it. */ }
