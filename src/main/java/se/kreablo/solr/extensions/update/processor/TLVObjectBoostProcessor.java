package se.kreablo.solr.extensions.update.processor;

import javax.inject.Named;
import javax.inject.Inject;
import java.util.Map;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.util.regex.Matcher;
import java.util.Collection;

import org.slf4j.Logger;

import org.xwiki.component.annotation.Component;
import org.xwiki.model.EntityType;
import org.xwiki.model.reference.EntityReference;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.DocumentReferenceResolver;
import org.xwiki.model.reference.EntityReferenceSerializer;
import org.xwiki.model.reference.ObjectReference;
import org.xwiki.model.reference.WikiReference;
import org.xwiki.bridge.DocumentAccessBridge;

import org.apache.solr.update.processor.UpdateRequestProcessor;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.SolrInputDocument;
import org.apache.solr.common.SolrInputField;



@Component
@Named("tlvObjectBoost")
public class TLVObjectBoostProcessor implements SolrUpdateRequestProcessor {

    @Inject
    private DocumentAccessBridge documentAccessBridge;

    @Inject
    private DocumentReferenceResolver<String> documentReferenceResolver;

    @Inject
    private EntityReferenceSerializer<String> entityReferenceSerializer;

    @Inject
    private Logger logger;

    private static final String CONFIGURATION_DOCUMENT_PARAMETER = "configurationDocument" ;

    private static final String CONFIGURATION_CLASS_PARAMETER = "configurationClass" ;

    private static final String DEFAULT_CONFIGURATION_DOCUMENT = "TLV.SolrObjectBoostConfiguration";

    private static final String DEFAULT_CONFIGURATION_CLASS = "TLV.SolrObjectBoostConfigurationClass";

    private static final String DEFAULT_TITLE_FIELD = "title_";

    private static final String DEFAULT_BOOST_FIELD = "objcontent_";

    private final Map<String,Map<String, Float>> boosts = new HashMap<String, Map<String, Float>>();

    private final static HashMap<String, Float> boostsDefault = new HashMap<String, Float>() {
        public static final long serialVersionUID=1;
        {
            put("condition_boost", 1.0f);
            put("condition_title_boost", 1.0f);
            put("measure_boost", 1.0f);
            put("measure_title_boost", 1.0f);
            put("terms_boost", 1.0f);
            put("terms_title_boost", 1.0f);
            put("question_boost", 1.0f);
            put("question_title_boost", 1.0f);
            put("advice_boost", 1.0f);
            put("advice_title_boost", 1.0f);
            put("handbook_boost", 1.0f);
            put("handbook_title_boost", 1.0f);
            put("paragraph_boost", 1.0f);
            put("paragraph_title_boost", 1.0f);
        }
    };

    private final Booster [] boosters = {
        new Booster("TLV.ConditionClass", "condition", "description"),
        new Booster("TLV.MeasureClass", "measure", "title"),
        new Booster("TLV.TermsClass", "terms", "title"),
        new Booster("TLV.FAQClass", "question"),
        new Booster("TLV.AppendixSectionClass", "advice", "title"),
        new Booster("TLV.RegulationParagraphClass", "paragraph"),
        new Booster(null, "handbook", null, "Handbok")
    };

    private String configDocName;

    private String configClass;

    @Override
    public void init(SolrParams solrParams, SolrQueryRequest request, SolrQueryResponse response)
    {
        if (solrParams != null) {
            configDocName = solrParams.get(CONFIGURATION_DOCUMENT_PARAMETER, DEFAULT_CONFIGURATION_DOCUMENT);
            configClass = solrParams.get(CONFIGURATION_CLASS_PARAMETER, DEFAULT_CONFIGURATION_CLASS);
        } else {
            configDocName = DEFAULT_CONFIGURATION_DOCUMENT;
            configClass = DEFAULT_CONFIGURATION_CLASS;
        }

        boosts.clear();
    }

    private Map<String, Float> getBoosts(String wiki)
    {
        Map<String, Float> wikiBoosts = boosts.get(wiki);

        if (wikiBoosts == null) {
            wikiBoosts = new HashMap<>(boostsDefault);
            boosts.put(wiki, wikiBoosts);
            for (String boost : boostsDefault.keySet()) {
                setBoost(boost, wiki, wikiBoosts);
            }
        }

        return wikiBoosts;
    }

    private void setBoost(String boost, String wiki, Map<String, Float> wikiBoosts)
    {

        DocumentReference configClassRef = documentReferenceResolver.resolve(configClass);
        DocumentReference configDocRef = documentReferenceResolver.resolve(configDocName);

        final WikiReference wikiRef = new WikiReference(wiki);

        configClassRef = configClassRef.replaceParent(configClassRef.extractReference(EntityType.WIKI), wikiRef);
        configDocRef = configDocRef.replaceParent(configDocRef.extractReference(EntityType.WIKI), wikiRef);

        ObjectReference configObjRef = new ObjectReference( entityReferenceSerializer.serialize(configClassRef), configDocRef );

        Object value = documentAccessBridge.getProperty(configObjRef, boost);
        Float boostValue = null;

        if (value != null) {
            if (value instanceof Number) {
                boostValue = ((Number)value).floatValue();
            } else if (value instanceof String && !value.equals("")) {
                try {
                    boostValue = Float.parseFloat((String) value);
                } catch (NumberFormatException e) {
                    logger.error("Failed to parse boost value '" + value + "' in configuration document (" + configObjRef + ") for " + boost, e);
                }
            } else {
                logger.warn("Unknown boost value '{}' in configuration document {} for {}.", value, configObjRef, boost );
            }
        }

        if (boostValue == null) {
            // logger.info("No boost value in configuration document {} for {}.", configObjRef, boost);
            return;
        }

        wikiBoosts.put(boost, boostValue);
    }

    @Override
    public void processAdd(AddUpdateCommand cmd)
    {
        final SolrInputDocument document = cmd.getSolrInputDocument();
        if (document.containsKey("wiki")) {
            final String wiki = document.getFieldValue("wiki").toString();
            for (Booster booster : boosters) {
                if (booster.boost(document, wiki)) {
                    break;
                }
            }
        }
    }

    private class Booster {

        private final String xwikiClass;

        protected final String boost;

        protected final String titleBoost;

        protected final String titleField;

        protected final String boostFieldPrefix;

        protected final String titleFieldPrefix;

        protected final String space;

        Booster(String xwikiClass, String boost)
        {
            this(xwikiClass, boost, null, null);
        }

        Booster(String xwikiClass, String boost, String titleField)
        {
            this(xwikiClass, boost, titleField, null);
        }

        Booster(String xwikiClass, String boost, String titleField, String space)
        {
            this.xwikiClass = xwikiClass;
            this.boost = boost + "_boost";
            this.titleBoost = boost + "_title_boost";
            this.titleField = titleField;
            this.space = space;

            if (xwikiClass != null) {
                boostFieldPrefix = "object." + xwikiClass + "_";
            } else {
                boostFieldPrefix = null;
            }

            if (titleField != null && xwikiClass != null) {
                titleFieldPrefix = "property." + xwikiClass +  "." + titleField + "_";
            } else {
                titleFieldPrefix = null;
            }

        }

        private void boost_(SolrInputDocument document, String fieldName, Float boostValue)
        {
            final SolrInputField field = document.getField(fieldName);
            if (field != null && boostValue != null) {
                logger.debug("We have a document with boost field {} boosting {}", fieldName, boostValue);
                logger.debug("All fields: {}", document.getFieldNames());
                field.setBoost(boostValue.floatValue());
            }
        }

        public boolean boost(SolrInputDocument document, String wiki)
        {
            if (document.containsKey("locale")) {
                final String spaceField = "space";
                final Object locale = document.getFieldValue("locale");
                if (locale == null) {
                    return false;
                }
                final String bf = boostFieldPrefix + locale;
                if (boostFieldPrefix != null && document.containsKey(bf)) {
                    // boost_(document, bf, getBoosts(wiki).get(boost));
                    boost_(document, "objcontent_" + locale, getBoosts(wiki).get(boost));
                } else if (document.containsKey(spaceField)) {
                    final Object space = document.getFieldValue(spaceField);
                    if (this.space != null && this.space.equals(space)) {
                        final String contentField = "doccontent_" + locale;
                        if (document.containsKey(contentField)) {
                            boost_(document, contentField, getBoosts(wiki).get(boost));
                        }
                    } else {
                        return false;
                    }
                }
                final String docTitleField = "title_" + locale;
                if (document.containsKey(docTitleField)) {
                    final Float tb = getBoosts(wiki).get(titleBoost);
                    boost_(document, docTitleField, tb);
                    if (boostFieldPrefix != null) {
                        final String tf = titleFieldPrefix + locale;
                        if (document.containsKey(tf)) {
                            logger.debug("Document contains title field {} appending to title", tf);
                            SolrInputField extraTitle = document.getField(tf);
                            SolrInputField title = document.getField(docTitleField);
                            if (title != null && extraTitle != null) {
                                final Object titleValue = title.getValue();
                                final Object extraValue = extraTitle.getValue();
                                if (titleValue instanceof Collection) {
                                    logger.debug( "Appending {} to original collection {}", extraValue , titleValue );
                                    title.addValue( extraValue, tb.floatValue() );
                                } else {
                                    logger.debug( "Appending {} to original string {}", extraValue, titleValue );
                                    title.setValue( titleValue.toString() + " " + extraValue, tb.floatValue() );
                                }

                            }
                        }
                    }
                }
            }
            return true;
        }
    }

}
