/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package se.kreablo.solr.extensions.update.processor;

import java.util.HashMap;
import java.util.Map;
import java.io.IOException;
import org.apache.solr.common.params.SolrParams;
import org.apache.solr.common.util.NamedList;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.update.processor.UpdateRequestProcessor;
import org.apache.solr.update.processor.UpdateRequestProcessorFactory;

import com.xpn.xwiki.web.Utils;

/**
 * Factory which creates TLV object boost processors
 *
 * The instantiated update request processor is a proxy to an XWiki
 * component SolrUpdateRequestProcessor.  (This class could be
 * generalized to take the hint from the solr parameters and thus act
 * as the proxy for multiple XWiki component type processors.)
 */
public class TLVObjectBoostProcessorFactory extends UpdateRequestProcessorFactory {

    private SolrParams params;
    private final Map<Object, Object> sharedObjectCache = new HashMap<Object, Object>();

    @Override
    public void init(@SuppressWarnings("rawtypes") final NamedList args) {
        if (args != null) {
            this.params = SolrParams.toSolrParams(args);
        }
    }

    @Override
    public UpdateRequestProcessor getInstance(SolrQueryRequest request, SolrQueryResponse response, UpdateRequestProcessor nextProcessor) {

        final SolrUpdateRequestProcessor p = Utils.getComponent(SolrUpdateRequestProcessor.class, "tlvObjectBoost");

        p.init(params, request, response);

        return new UpdateRequestProcessor(nextProcessor) {
            @Override
            public void processAdd(AddUpdateCommand cmd) throws IOException
            {
                p.processAdd(cmd);
                super.processAdd(cmd);
            }
        };

    }
}
