package se.kreablo.solr.extensions.update.processor;

import java.util.Map;
import java.util.Set;

import org.apache.lucene.analysis.TokenFilter;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.util.TokenFilterFactory;

public class TLVItemFilterFactory extends TokenFilterFactory {

    private final Set<Integer> types;

    /** Creates a new ClassicFilterFactory */
    public TLVItemFilterFactory(Map<String,String> args) {
        super(args);

        types = null;

        if (!args.isEmpty()) {
            throw new IllegalArgumentException("Unknown parameters: " + args);
        }
    }

    @Override
    public TokenFilter create(TokenStream input) {
        return null;
    }

}
