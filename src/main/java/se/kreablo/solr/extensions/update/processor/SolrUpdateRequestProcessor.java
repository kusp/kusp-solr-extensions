package se.kreablo.solr.extensions.update.processor;

import java.io.IOException;

import org.xwiki.component.annotation.Role;

import org.apache.solr.update.AddUpdateCommand;
import org.apache.solr.request.SolrQueryRequest;
import org.apache.solr.response.SolrQueryResponse;
import org.apache.solr.common.params.SolrParams;

/**
 * Component role for processing solr update requests.
 */
@Role
public interface SolrUpdateRequestProcessor {

    void init(SolrParams solrParams, SolrQueryRequest request, SolrQueryResponse response);

    void processAdd(AddUpdateCommand cmd) throws IOException;

}
