package se.kreablo.lucene.extensions.virtual;

import java.io.Reader;
import java.io.FilterReader;

public class WikiReader extends FilterReader implements VirtualWikiSupport {

    private String wiki;

    public WikiReader(Reader in)
    {
        super(in);
    }

    public String getWiki()
    {
        return wiki;
    }

    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }

}
