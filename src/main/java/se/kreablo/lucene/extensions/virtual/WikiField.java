package se.kreablo.lucene.extensions.virtual;

import java.io.Reader;

import org.apache.lucene.document.Field;

public class WikiField extends Field implements VirtualWikiSupport {

    private String wiki;

    public WikiField(String name, String val, org.apache.lucene.document.FieldType type)
    {
        super(name, val, type);
    }

    @Override
    public String getWiki()
    {
        return wiki;
    }

    @Override
    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }

    @Override
    public Reader readerValue() {
        final Reader inner = super.readerValue();
        Reader wikiReader;

        if (inner != null) {
            WikiReader r = new WikiReader(inner);
            r.setWiki(getWiki());
            wikiReader = r;
        } else {
            WikiStringReader r = new WikiStringReader(stringValue());
            r.setWiki(getWiki());
            wikiReader = r;
        }

        return wikiReader;
    }
}
