package se.kreablo.lucene.extensions.virtual;

import com.xpn.xwiki.web.Utils;
import com.xpn.xwiki.XWikiContext;
import org.xwiki.context.Execution;
import org.xwiki.context.ExecutionContext;

/**
 * This is a terrible hack for obtaining the current virtual wiki from
 * lucene core.  It depends on having the SOLR indexer thread set the
 * virtual wiki via the WikiStrFieldType type of field.  Query
 * requests relies on the execution context.
 */
public class VirtualWiki {

    private static final ThreadLocal<String> virtualWiki = new ThreadLocal<String>();

    public static String getWiki()
    {
        String wiki = virtualWiki.get();
        if (wiki != null) {
            return wiki;
        }

        Execution execution = Utils.getComponent(Execution.class);
        if (execution == null) {
            return null;
        }

        ExecutionContext econtext = execution.getContext();
        if (econtext == null) {
            return null;
        }

        XWikiContext xwikiContext = (XWikiContext) execution.getContext().getProperty("xwikicontext");
        if (xwikiContext == null) {
            return null;
        }

        return xwikiContext.getWikiId();
    }

    public static void setWiki(String wiki)
    {
        virtualWiki.set(wiki);
    }

}
