package se.kreablo.lucene.extensions.virtual;

import java.io.Reader;
import java.io.StringReader;

public class WikiStringReader extends StringReader implements VirtualWikiSupport {

    private String wiki;

    public WikiStringReader(String s)
    {
        super(s);
    }

    public String getWiki()
    {
        return wiki;
    }

    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }
}
