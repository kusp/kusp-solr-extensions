package se.kreablo.lucene.extensions.virtual;

import org.apache.solr.schema.StrField;
import org.apache.lucene.index.IndexableField;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class WikiStrFieldType extends StrField {

    private final Logger logger = LoggerFactory.getLogger(WikiStrFieldType.class);

    @Override
    protected IndexableField createField(String name, String val, org.apache.lucene.document.FieldType type, float boost)
    {
        VirtualWiki.setWiki(val);
        return super.createField(name, val, type, boost);
    }
}
