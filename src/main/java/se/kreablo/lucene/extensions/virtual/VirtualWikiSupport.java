package se.kreablo.lucene.extensions.virtual;


/**
 * We need to extend some fields to pass around the wiki name.
 */
public interface VirtualWikiSupport {

    void setWiki(String wiki);

    String getWiki();
}
