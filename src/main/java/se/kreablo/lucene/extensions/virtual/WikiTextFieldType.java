package se.kreablo.lucene.extensions.virtual;

import org.apache.solr.schema.TextField;
import org.apache.lucene.index.IndexableField;

public class WikiTextFieldType extends TextField implements VirtualWikiSupport {

    private String wiki;

    @Override
    public String getWiki()
    {
        return wiki;
    }

    @Override
    public void setWiki(String wiki)
    {
        this.wiki = wiki;
    }

    @Override
    protected IndexableField createField(String name, String val, org.apache.lucene.document.FieldType type, float boost){
        WikiField f = new WikiField(name, val, type);
        f.setBoost(boost);
        f.setWiki(getWiki());
        return f;
    }

}

