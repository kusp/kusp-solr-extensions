package se.kreablo.lucene.extensions.analyse;

import java.io.IOException;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.TokenFilter;

public final class TLVTermsCodeFilter extends TokenFilter {

    public TLVTermsCodeFilter(TokenStream input) {
        super(input);
    }

    @Override
    public boolean incrementToken()
        throws IOException
    {
        return input.incrementToken();
    }

}
