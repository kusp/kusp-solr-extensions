package se.kreablo.lucene.extensions.analyse.synonym;

import org.xwiki.component.annotation.Role;

import org.apache.lucene.analysis.synonym.SynonymMap;
import org.apache.lucene.analysis.util.ResourceLoader;

@Role
public interface SynonymHolder {

    SynonymMap getSynonymMap(boolean expanded);

}
