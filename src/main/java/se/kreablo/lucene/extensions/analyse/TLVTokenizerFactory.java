package se.kreablo.lucene.extensions.analyse;

import org.apache.lucene.analysis.util.TokenizerFactory;
import org.apache.lucene.util.AttributeFactory;
import org.apache.lucene.analysis.standard.StandardAnalyzer;

import java.io.Reader;
import java.util.Map;

public class TLVTokenizerFactory extends TokenizerFactory {

    /** Creates a new StandardTokenizerFactory */
    public TLVTokenizerFactory(Map<String,String> args) {
        super(args);
        assureMatchVersion();
        if (!args.isEmpty()) {
            throw new IllegalArgumentException("Unknown parameters: " + args);
        }
    }

    @Override
    public TLVTokenizer create(AttributeFactory factory, Reader input) {
        TLVTokenizer tokenizer = new TLVTokenizer(factory, input);
        return tokenizer;
    }
}
