package se.kreablo.lucene.extensions.analyse.synonym;

import java.util.List;
import java.util.Map;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.regex.Pattern;
import java.io.IOException;
import java.io.Reader;
import java.io.StringReader;
import java.text.ParseException;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;

import org.xwiki.component.annotation.Component;
import org.xwiki.model.EntityType;
import org.xwiki.model.reference.EntityReference;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.DocumentReferenceResolver;
import org.xwiki.model.reference.EntityReferenceSerializer;
import org.xwiki.model.reference.ObjectReference;
import org.xwiki.model.reference.WikiReference;
import org.xwiki.bridge.DocumentAccessBridge;
import org.xwiki.observation.EventListener;
import org.xwiki.observation.event.Event;
import org.xwiki.bridge.event.DocumentUpdatedEvent;
import org.xwiki.observation.event.filter.RegexEventFilter;

import se.kreablo.lucene.extensions.virtual.VirtualWiki;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.analysis.Tokenizer;
import org.apache.lucene.analysis.Analyzer.TokenStreamComponents;
import org.apache.lucene.analysis.core.LowerCaseFilter;
import org.apache.lucene.analysis.core.WhitespaceTokenizer;
import org.apache.lucene.analysis.synonym.SynonymFilter;
import org.apache.lucene.analysis.synonym.SynonymMap;
import org.apache.lucene.analysis.synonym.SolrSynonymParser;
import org.apache.lucene.analysis.synonym.WordnetSynonymParser;

import org.apache.lucene.util.Version;

@Component
@Singleton
public class TLVSynonymHolder implements SynonymHolder, EventListener {

    private final String CLASSNAME = "TLV.LuceneSynonymsClass";

    private final String PROPERTY = "synonyms";

    private final String DOCNAME = "TLV.LuceneSynonyms";

    private final Event[] events = { new DocumentUpdatedEvent(new RegexEventFilter("^.*:" + Pattern.quote(DOCNAME))) };

    private final List<Event> eventList = Collections.unmodifiableList(Arrays.asList(events));

    private final Map<String, SynonymMap> expandedSynonyms = new HashMap<>();

    private final Map<String, SynonymMap> synonyms = new HashMap<>();

    @Inject
    private DocumentAccessBridge documentAccessBridge;

    @Inject
    private DocumentReferenceResolver<String> documentReferenceResolver;

    @Inject
    private EntityReferenceSerializer<String> entityReferenceSerializer;

    @Inject
    private Logger logger;

    @Override
    public String getName()
    {
        return "tlv-synonym-holder";
    }

    @Override
    public List<Event> getEvents()
    {
        return eventList;
    }

    @Override
    public synchronized void onEvent(Event event, Object source, Object data)
    {
        synonyms.clear();
    }

    @Override
    public synchronized SynonymMap getSynonymMap(boolean expanded)
    {
        final String wiki = VirtualWiki.getWiki();

        SynonymMap synonymMap = (expanded ? expandedSynonyms : synonyms).get( wiki );

        if (synonymMap == null) {
            final String content = getPropertyContent(DOCNAME, CLASSNAME, PROPERTY);

            if (content == null) {
                return null;
            }

            final Analyzer analyzer = new Analyzer() {
                    @Override
                    protected TokenStreamComponents createComponents(String fieldName, Reader reader) {
                        Tokenizer tokenizer = new WhitespaceTokenizer(Version.LUCENE_CURRENT, reader);
                        TokenStream stream = new LowerCaseFilter(Version.LUCENE_CURRENT, tokenizer);
                        return new TokenStreamComponents(tokenizer, stream);
                    }
                };

            try {
                logger.debug("Logging synonyms from content '{}'", content);
                synonymMap = loadSynonyms(true, analyzer, content, expanded);
            } catch (ParseException e) {
                logger.error("Error parsing synonyms file", e);
                return null;
            } catch (IOException e) {
                logger.error("Error parsing synonyms file", e);
                return null;
            }

            (expanded ? expandedSynonyms : synonyms).put(wiki, synonymMap);
        }

        return synonymMap;

    }

    private String getPropertyContent(String documentName, String className, String property)
    {
        DocumentReference classRef = documentReferenceResolver.resolve(className);
        DocumentReference docRef = documentReferenceResolver.resolve(documentName);

        final String wiki = VirtualWiki.getWiki();
        if (wiki == null) {
            logger.error("No virtual wiki set!");
            return null;
        }

        logger.debug("Virtual wiki: {}", wiki);

        final WikiReference wikiRef = new WikiReference(wiki);

        classRef = classRef.replaceParent(classRef.extractReference(EntityType.WIKI), wikiRef);
        docRef = docRef.replaceParent(docRef.extractReference(EntityType.WIKI), wikiRef);

        ObjectReference objRef = new ObjectReference( entityReferenceSerializer.serialize(classRef), docRef );

        Object value = documentAccessBridge.getProperty(objRef, property);
        if (value == null) {
            logger.error("No value for property {} {}", objRef, property);
            return null;
        }

        return value.toString();
    }

    /**
     * Load synonyms with the given {@link SynonymMap.Parser} class.
     */
    private SynonymMap loadSynonyms(boolean dedup, Analyzer analyzer, String content, boolean expand) throws IOException, ParseException {
        SynonymMap.Parser parser = new SolrSynonymParser(dedup, expand, analyzer);
        parser.parse(new StringReader(content));

        return parser.build();
    }

}
