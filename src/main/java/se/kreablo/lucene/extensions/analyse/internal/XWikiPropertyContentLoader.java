package se.kreablo.lucene.extensions.analyse.internal;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.slf4j.Logger;

import org.xwiki.component.annotation.Component;
import org.xwiki.model.EntityType;
import org.xwiki.model.reference.EntityReference;
import org.xwiki.model.reference.DocumentReference;
import org.xwiki.model.reference.DocumentReferenceResolver;
import org.xwiki.model.reference.EntityReferenceSerializer;
import org.xwiki.model.reference.ObjectReference;
import org.xwiki.model.reference.WikiReference;
import org.xwiki.bridge.DocumentAccessBridge;

import se.kreablo.lucene.extensions.virtual.VirtualWiki;
import se.kreablo.lucene.extensions.analyse.PropertyContentLoader;

@Component
@Singleton
public class XWikiPropertyContentLoader implements PropertyContentLoader {

    @Inject
    private DocumentAccessBridge documentAccessBridge;

    @Inject
    private DocumentReferenceResolver<String> documentReferenceResolver;

    @Inject
    private EntityReferenceSerializer<String> entityReferenceSerializer;

    @Inject
    Logger logger;

    @Override
    public String getPropertyContent(String documentName, String className, String property)
    {
        DocumentReference classRef = documentReferenceResolver.resolve(className);
        DocumentReference docRef = documentReferenceResolver.resolve(documentName);

        final String wiki = VirtualWiki.getWiki();
        if (wiki == null) {
            logger.error("No virtual wiki set!");
            return null;
        }

        logger.debug("Virtual wiki: {}", wiki);

        final WikiReference wikiRef = new WikiReference(wiki);

        classRef = classRef.replaceParent(classRef.extractReference(EntityType.WIKI), wikiRef);
        docRef = docRef.replaceParent(docRef.extractReference(EntityType.WIKI), wikiRef);

        ObjectReference objRef = new ObjectReference( entityReferenceSerializer.serialize(classRef), docRef );

        Object value = documentAccessBridge.getProperty(objRef, property);
        if (value == null) {
            logger.error("No value for property {} {}", objRef, property);
            return null;
        }

        return value.toString();
    }

}
