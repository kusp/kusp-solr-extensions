package se.kreablo.lucene.extensions.analyse;

import org.xwiki.component.annotation.Role;

@Role
public interface PropertyContentLoader {

    String getPropertyContent(String document, String classDocument, String property);

}
