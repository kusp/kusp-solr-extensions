package se.kreablo.lucene.extensions.analyse.synonym;
/*
 * Licensed to the Apache Software Foundation (ASF) under one or more
 * contributor license agreements.  See the NOTICE file distributed with
 * this work for additional information regarding copyright ownership.
 * The ASF licenses this file to You under the Apache License, Version 2.0
 * (the "License"); you may not use this file except in compliance with
 * the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import java.io.IOException;
import java.io.StringReader;
import java.io.Reader;
import java.nio.charset.Charset;
import java.nio.charset.CharsetDecoder;
import java.nio.charset.CodingErrorAction;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.apache.lucene.analysis.TokenStream;
import org.apache.lucene.util.Version;
import org.apache.lucene.analysis.util.TokenFilterFactory;

import se.kreablo.lucene.extensions.analyse.PropertyContentLoader;

/**
 */
public class TLVSynonymFilterFactory extends TokenFilterFactory {
    private final boolean expand;
    private final Map<String, String> tokArgs = new HashMap<>();

    private final Logger logger = LoggerFactory.getLogger(TLVSynonymFilterFactory.class);

    public TLVSynonymFilterFactory(Map<String,String> args) {
        super(args);
        expand = getBoolean(args, "expand", true);

        if (!args.isEmpty()) {
            throw new IllegalArgumentException("Unknown parameters: " + args);
        }
    }

    @Override
    public TokenStream create(TokenStream input) {
        logger.debug("Creating tlv synonym filter.");
        TLVSynonymFilter synonymFilter = new TLVSynonymFilter(input, expand);
        return synonymFilter;
    }
}
