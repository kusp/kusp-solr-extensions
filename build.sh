#!/bin/sh

dir="$(cd "$(dirname "$BASH_SOURCE")"; pwd -P)"

extName="$(basename "$dir")"

rsync -tr --exclude .git --exclude \*~  --delete "$dir" wiki:hdb/lucene

ssh -X wiki 'cd hdb/lucene/kreablo-extensions &&  ~/apache-maven/bin/mvn package && scp target/kreablo*.jar tlv.kreablo.se:tmp && ssh -X tlv.kreablo.se SUDO_ASKPASS=/usr/bin/ssh-askpass sudo --askpass mv tmp/kreablo*.jar /var/lib/tomcat8/webapps/ROOT/WEB-INF/lib'
